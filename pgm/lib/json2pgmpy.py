import json
from pgmpy.models import BayesianModel
from pgmpy.factors.continuous.
from pgmpy.estimators import MaximumLikelihoodEstimator, BayesianEstimator
import pandas as pd

def json2pgmpy(json_data, Model=BayesianModel):
    model = Model()
    nodes = {}
    for node in json_data['nodes']:
        model.add_node(node['name'].split(' ')[0])
        nodes[node['id']] = node['name']
    
    for link in json_data['links']:
        source = nodes[link['source']].split(' ')[0]
        target = nodes[link['target']].split(' ')[0]
        model.add_edge(source, target)

    print(model.nodes())
    return model

def train(model, df):
    model.fit(df, estimator=MaximumLikelihoodEstimator)
    return model