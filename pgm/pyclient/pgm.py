import requests
from io import BytesIO
import json


class pgm():
    def __init__(self, project={'api_root': ''}):
        from pgmpy.models import BayesianModel
        from pgmpy.estimators import MaximumLikelihoodEstimator, BayesianEstimator

        self.project = project

    def list(self, path: str = ''):
        if not path.startswith('/'):
            path = '/'+path
        url = self.project['api_root'] + 'pgm/pgms/' + \
            str(self.project['name']) + path
        return requests.get(url).json()['pgms']

    def json(self, file_name: str):
        if not file_name.startswith('/'):
            file_name = '/'+file_name
        url = self.project['api_root'] + 'pgm/json/' + \
            str(self.project['name']) + file_name
        response = requests.get(url)
        return json.loads(response.content)

    def download(self, file_name: str):
        if not file_name.startswith('/'):
            file_name = '/'+file_name
        url = self.project['api_root'] + 'csv/download/' + \
            str(self.project['name']) + file_name
        response = requests.get(url)
        return BytesIO(response.content)

    def pgmpy(self, file_name: str, Model):
        json_ = self.json(file_name)
        return self.json2pgmpy(json_['data'], Model)

    def json2pgmpy(self, json_data, Model):

        model = Model()
        nodes = {}
        for node in json_data.get('nodes') or []:
            model.add_node(node['name'].split(' ')[0])
            nodes[node['id']] = node['name']

        for link in json_data.get('links') or []:
            source = nodes[link['source']].split(' ')[0]
            target = nodes[link['target']].split(' ')[0]
            model.add_edge(source, target)
        return model

    def pgmpy2json(self, model, file_name: str):
        json_ = self.json(file_name)

        json_nodes = []
        for node in model.nodes():
            json_nodes.append({'id': node, 'name': node, 'type': 'default'})
        json_['data']['nodes'] = json_nodes

        json_edges = []
        for edge in model.edges():
            json_edges.append({'id': str(edge), 'type': 'default', 'source': edge[0],
                               'target': edge[1]})
        json_['data']['links'] = json_edges
        return json_
