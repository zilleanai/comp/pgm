import os
import yaml
import uuid
import json
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig

from bundles.project.models import Project
from bundles.project.services import ProjectManager
from bundles.tag.services import TagManager
from bundles.file.services import FileManager
from bundles.file.models import File


def allowed_file(filename):
    return filename.endswith('_pgm.json')


class PGMController(Controller):

    file_manager: FileManager = injectable
    project_manager: ProjectManager = injectable
    tag_manager: TagManager = injectable

    @route('/pgms/<string:project>', defaults={'id': ''})
    @route('/pgms/<string:project>/<path:id>')
    def pgms(self, project, id):
        abs_path = os.path.join(AppConfig.DATA_FOLDER, project, id)
        if not id:
            pgms = [f.replace('_pgm.json', '')
                    for f in os.listdir(abs_path) if allowed_file(f)]
            resp = jsonify(project=project, pgms=pgms)
            return resp
        else:
            filename = str(id) + '_pgm.json'
            abs_path = os.path.join(AppConfig.DATA_FOLDER, project, filename)
            return self.json(project, id)
        return abort(404)

    @route('/download/<string:project>/<string:id>')
    def download(self, project, id):
        filename = str(id) + '_pgm.json'
        filename = os.path.join(AppConfig.DATA_FOLDER, project, filename)
        if os.path.isfile(filename):
            return send_file(filename)
        return abort(404)

    @route('/json/<string:project>/<string:id>')
    def json(self, project, id):
        filename = str(id) + '_pgm.json'
        filename = os.path.join(AppConfig.DATA_FOLDER, project, filename)
        with open(filename, 'r') as infile:
            data = json.load(infile)
        distributions = {}
        filename = str(id) + '_distributions.json'
        filename = os.path.join(AppConfig.DATA_FOLDER, project, filename)
        if os.path.exists(filename):
            with open(filename, 'r') as infile:
                distributions = json.load(infile)
        resp = jsonify(id=id, data=data, distributions=distributions)
        return resp

    @route('/save/<string:project>/<string:id>', methods=['POST'])
    def save(self, project, id):
        pgm = request.json['pgm']
        id = pgm['id']
        filename = str(id) + '_pgm.json'
        with open(os.path.join(AppConfig.DATA_FOLDER, project, filename), 'w') as outfile:
            json.dump(json.loads(pgm['data']), outfile)
        filename = str(id) + '_distributions.json'
        with open(os.path.join(AppConfig.DATA_FOLDER, project, filename), 'w') as outfile:
            json.dump(pgm['distributions'], outfile)
        resp = jsonify(success=True)
        return resp

    @route('/new-pgm/<string:project>', methods=['POST'])
    def new(self, project):
        id = uuid.uuid4()
        filename = str(id) + '_pgm.json'
        empty_model = {
            "id": str(id),
            "offsetX": 0,
            "offsetY": 0,
            "zoom": 100,
            "gridSize": 0,
            "links": [],
            "nodes": [{'color': "rgb(192,64,32)",
                       'extras': {},
                       'id': "0",
                       'name': "action",
                       'ports': [{
                           'id': "1",
                           'in': True,
                           'label': "X",
                           'links': [],
                           'maximumLinks': None,
                           'name': "1",
                           'parentNode': "0",
                           'selected': False,
                           'type': "default",
                       }],
                       'selected': False,
                       'type': "default",
                       'x': 100,
                       'y': 100, }]
        }

        with open(os.path.join(AppConfig.DATA_FOLDER, project, filename), 'w') as outfile:
            json.dump(empty_model, outfile)

        resp = jsonify(success=True)
        return resp
