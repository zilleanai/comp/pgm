"""
    pgm
    ~~~~

    Component for pgm in general.

    :copyright: Copyright © 2018 chriamue
    :license: Not open source, see LICENSE for details
"""

__version__ = '0.1.0'


from flask_unchained import Bundle


class PGM(Bundle):
    command_group_names = ['pgm']