# pgm

probability graphical model component

## Installation

```yml
# mlplatform-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/pgm
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.pgm',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.pgm.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Modeler,
  PGMs
} from 'comps/pgm/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  Modeler: 'Modeler',
  PGMs: 'PGMs',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.Modeler,
    path: '/modeler/:id',
    component: Modeler,
  },
  {
    key: ROUTES.PGMs,
    path: '/pgms',
    component: PGMs,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.PGMs} />
    ...
</div>
```