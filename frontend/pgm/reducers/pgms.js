import { listPGMs } from 'comps/pgm/actions'


export const KEY = 'pgms'

const initialState = {
  isLoading: false,
  isLoaded: false,
  ids: [],
  byId: {},
  error: null,
}

export default function(state = initialState, action) {
  const { type, payload } = action
  const { pgms } = payload || {}
  const { byId } = state
  switch (type) {
    case listPGMs.REQUEST:
      return { ...state,
        isLoading: true,
      }

    case listPGMs.SUCCESS:
      return { ...state,
        ids: pgms.map((pgm) => pgm),
        byId: pgms.reduce((byId, pgm) => {
          byId[pgm] = pgm
          return byId
        }, byId),
        isLoaded: true,
      }

    case listPGMs.FAILURE:
      return { ...state,
        error: payload.error,
      }

    case listPGMs.FULFILL:
      return { ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectPGMs = (state) => state[KEY]
export const selectPGMsList = (state) => {
  const pgms = selectPGMs(state)
  return pgms.ids.map((id) => pgms.byId[id])
}
