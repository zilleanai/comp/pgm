import { loadPGMData } from 'comps/pgm/actions'


export const KEY = 'pgm'

const initialState = {
  isLoading: false,
  ids: [],
  byId: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { pgm } = payload || {}
  const { ids, byId } = state

  switch (type) {
    case loadPGMData.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case loadPGMData.SUCCESS:
      if (!ids.includes(pgm.id)) {

        ids.push(pgm.id)
      }
      byId[pgm.id] = pgm
      return {
        ...state,
        ids,
        byId,
      }

    case loadPGMData.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectPGMData = (state) => state[KEY]
export const selectPGMDataById = (state, id) => selectPGMData(state).byId[id]
