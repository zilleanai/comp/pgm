import { get, post } from 'utils/request'
import { v1 } from 'api'
import { storage } from 'comps/project'

function pgm(uri) {
    return v1(`/pgm${uri}`)
}

export default class PGM {
    static listPGMs() {
        return get(pgm(`/pgms/${storage.getProject()}/`))
    }

    static newPGM() {
        return post(pgm(`/new-pgm/${storage.getProject()}`))
    }
    /**
    * @param {Object} pgm
    */
    static loadPGMData(pgm_) {
        return get(pgm(`/json/${storage.getProject()}/${pgm_.id}`))
    }

    /**
    * @param {Object} pgm
    */
    static savePGMData({ pgm_ }) {
        return post(pgm(`/save/${storage.getProject()}/${pgm_.id}`), { 'pgm': pgm_ })
    }
}