import { call, put, takeLatest } from 'redux-saga/effects'

import { savePGMData } from 'comps/pgm/actions'
import { createRoutineSaga } from 'sagas'
import PGMApi from 'comps/pgm/api'


export const KEY = 'savePGMData'

export const savePGMDataSaga = createRoutineSaga(
  savePGMData,
  function* successGenerator(payload) {
    payload = { 'pgm_': payload.pgm }
    const response = yield call(PGMApi.savePGMData, payload)
    yield put(savePGMData.success(response))
  }
)

export default () => [
  takeLatest(savePGMData.TRIGGER, savePGMDataSaga),
]
