import { call, put, takeLatest } from 'redux-saga/effects'

import { newPGM } from 'comps/pgm/actions'
import { createRoutineSaga } from 'sagas'
import PGMApi from 'comps/pgm/api'


export const KEY = 'newPGM'

export const newPGMSaga = createRoutineSaga(
  newPGM,
  function *successGenerator(payload) {
    payload = {}
    const response = yield call(PGMApi.newPGM, payload)
    yield put(newPGM.success(response))}
)

export default () => [
  takeLatest(newPGM.TRIGGER, newPGMSaga),
]
