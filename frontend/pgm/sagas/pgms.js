import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { listPGMs } from 'comps/pgm/actions'
import PGMApi from 'comps/pgm/api'
import { selectPGMs } from 'comps/pgm/reducers/pgms'


export const KEY = 'pgms'

export const maybeListPGMsSaga = function *() {
  const { isLoading, isLoaded } = yield select(selectPGMs)
  if (!(isLoaded || isLoading)) {
    yield put(listPGMs.trigger())
  }
}

export const listPGMsSaga = createRoutineSaga(
  listPGMs,
  function *successGenerator() {
    const pgms = yield call(PGMApi.listPGMs)
    yield put(listPGMs.success({
      pgms: pgms.pgms,
      project: pgms.project
    }))
  },
)

export default () => [
  takeEvery(listPGMs.MAYBE_TRIGGER, maybeListPGMsSaga),
  takeLatest(listPGMs.TRIGGER, listPGMsSaga),
]
