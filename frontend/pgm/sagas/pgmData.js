import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { loadPGMData } from 'comps/pgm/actions'
import PGMApi from 'comps/pgm/api'
import { selectPGMData } from 'comps/pgm/reducers/pgmData'


export const KEY = 'pgmData'

export const maybeLoadPGMDataSaga = function* (pgm) {
  const { byId, isLoading } = yield select(selectPGMData)
  const isLoaded = !!byId[pgm.id]
  if (!(isLoaded || isLoading)) {
    yield put(loadPGMData.trigger(pgm))
  }
}

export const loadPGMDataSaga = createRoutineSaga(
  loadPGMData,
  function* successGenerator({ payload: pgm }) {
    pgm = yield call(PGMApi.loadPGMData, pgm)
    yield put(loadPGMData.success({ pgm }))
  }
)

export default () => [
  takeEvery(loadPGMData.MAYBE_TRIGGER, maybeLoadPGMDataSaga),
  takeLatest(loadPGMData.TRIGGER, loadPGMDataSaga),
]
