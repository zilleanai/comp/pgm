import React from 'react'
import Helmet from 'react-helmet'

import { PageContent } from 'components'
import { PGMList } from 'comps/pgm/components'

export default () => (
  <PageContent>
      <Helmet>
        <title>Probability Graphical Models</title>
      </Helmet>
      <h1>Probability Graphical Models!</h1>
      <PGMList />
  </PageContent>
)
