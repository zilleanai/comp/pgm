import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'

import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { Button } from 'react-bootstrap';
import { PageContent } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { Application, BodyWidget, Distribution } from 'comps/pgm/components'
import { loadPGMData, savePGMData } from 'comps/pgm/actions'
import { selectPGMDataById } from 'comps/pgm/reducers/pgmData'
import { DiagramModel } from "storm-react-diagrams";
import { TagSelect } from 'comps/tag/components'

import './modeler.scss'

class Modeler extends React.Component {

  constructor(props) {
    super(props);

    var app = new Application();

    this.state = {
      checkedFeatures: new Map(),
      app: app,
      features: [],
      stepsEnabled: true,
      initialStep: 0,
      distributions: null,
      steps: [
        {
          element: '.modeler-page',
          intro: 'A graphical model can be created on this page. Graphical models can be used as probabilistic graphical models.',
        },
        {
          element: '.modeler-graph',
          intro: 'A graph can be modeled here.',
        },
        {
          element: '.modeler-tags',
          intro: 'Tags selected in this list can be used as nodes in the directed graph.',
        },
      ],
    }
    this.handleFeaturesChange = this.handleFeaturesChange.bind(this);
  }

  handleFeaturesChange({ value }) {
    this.setState({ features: value });
  }

  componentWillMount() {
    const { loadPGMData, id } = this.props
    loadPGMData.maybeTrigger({ id })
  }

  handleDistributionsChange = (variable, distname, param1, param2, param3) => {
    const { distributions } = this.state
    distributions[variable] = {
      variable, distname, param1, param2, param3
    }
    this.setState({ distributions: distributions })
  }

  render() {
    const { id, isLoaded, error, pristine, submitting, pgm } = this.props
    const { app, features, distributions } = this.state
    if (!isLoaded) {
      return null
    }
    if (!distributions && pgm.distributions) {
      const distributions = pgm.distributions

      const features = []
      for (var key in distributions) {
        features.push(distributions[key].variable)
      }
      this.setState({ features, distributions })
    }
    const { stepsEnabled, steps, initialStep } = this.state
    const engine = app.getDiagramEngine()
    var model = new DiagramModel()
    model.deSerializeDiagram(pgm.data, engine);
    engine.setDiagramModel(model);
    return (
      <PageContent className='modeler-page'>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Modeler</title>
        </Helmet>
        <div className="row">
          <label>PGM ID</label>
          <input readOnly value={id} /><br/>
          <button type="submit"
            className="button-primary"
            onClick={(e) => {
              const pgm = {
                'pgm': {
                  id: id,
                  distributions: distributions,
                  data: JSON.stringify(model.serializeDiagram())
                }
              }
              this.props.savePGMData.trigger(pgm)
            }}
            disabled={pristine || submitting}
          >
            {submitting ? 'Saving...' : 'Save'}
          </button>
        </div>
        <div className='modeler-tags'>
          <TagSelect
            defaultValue={features}
            onChange={this.handleFeaturesChange} />
        </div>
        <div className='modeler-graph'>
          <BodyWidget app={app} items={features} data={pgm.data} />
        </div>
        <div>
          <a href='https://statisticsblog.com/probability-distributions/'>distributions doc</a>
          <ul className="distributions" >
            {features.map((feature, idx) => {
              return (
                <li key={`distribution-${feature}`}><Distribution variable={feature} defaultValue={pgm.distributions[feature]} onChange={this.handleDistributionsChange} /></li>)
            })}
          </ul>
        </div>
      </PageContent>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    const id = props.match.params.id
    const pgm = selectPGMDataById(state, id)
    return {
      id,
      pgm,
      isLoaded: !!pgm,
    }
  },
  (dispatch) => bindRoutineCreators({ loadPGMData, savePGMData }, dispatch),
)

const withReducer = injectReducer(require('comps/pgm/reducers/pgmData'))
const withSaga = injectSagas(require('comps/pgm/sagas/pgmData'))
const withSaga2 = injectSagas(require('comps/pgm/sagas/savePGMData'))

export default compose(
  withReducer,
  withSaga,
  withSaga2,
  withConnect,
)(Modeler)
