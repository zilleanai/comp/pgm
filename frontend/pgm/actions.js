import { createRoutine } from 'actions'
export const listPGMs = createRoutine('pgm/LIST_PGMS')
export const newPGM = createRoutine('pgm/NEW_PGM')
export const loadPGMData = createRoutine('pgm/LOAD_PGM_DATA')
export const savePGMData = createRoutine('pgm/SAVE_PGM_DATA')