import React from 'react'
import { compose } from 'redux'
import Select from 'react-select';
import classnames from 'classnames'

import PD from 'probability-distributions'
import Plot from 'react-plotly.js';

import './distribution.scss'

class Distribution extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      distnames: ['Binomial', 'Beta', 'Cauchy', 'Normal', 'Uniform', 'Poisson'],
      distname: props.defaultValue?props.defaultValue.distname:'Normal',
      param1: props.defaultValue?props.defaultValue.param1:100,
      param2: props.defaultValue?props.defaultValue.param2:0,
      param3: props.defaultValue?props.defaultValue.param3:0.5
    }
  }

  createDist(distname, p1, p2, p3) {
    try {
      if (distname == 'Binomial') {
        return PD.rbinom(p1, p2, p3)
      }
      if (distname == 'Beta') {
        return PD.rbeta(p1, p2, p3)
      }
      if (distname == 'Cauchy') {
        return PD.rcauchy(p1, p2, p3)
      }
      if (distname == 'Normal') {
        return PD.rnorm(p1, p2, p3)
      }
      if (distname == 'Uniform') {
        return PD.runif(p1, p2, p3)
      }
      if (distname == 'Poisson') {
        return PD.rpois(p1, p2, p3)
      }
    } catch (e) {
      console.log(e)
    }
    return []
  }

  plotData(data) {
    return [{
      x: data,
      type: 'histogram',
    }]
  }

  toOptions(list) {
    var options = []
    list.forEach(function (value, key) {
      options.push({ value: value, label: value })
    }, list)
    return options
  }

  handleDistributionChange = (selectedOption) => {
    const { variable, onChange } = this.props
    const { distname, param1, param2, param3 } = this.state
    this.setState({ distname: selectedOption['value'] });
    onChange && onChange(variable, distname, param1, param2, param3)
  }

  render() {
    const { variable, onChange } = this.props
    const { distnames, distname, param1, param2, param3 } = this.state
    const data = this.createDist(distname, param1, param2, param3)
    return (
      <div className="distribution">
        <h3>{variable}-{distname}</h3>
        <Select
          value={{ value: distname, label: distname }}
          onChange={this.handleDistributionChange}
          defaultValue={distnames[0]}
          name="distributions"
          options={this.toOptions(distnames)}
          className="basic-single-select"
          classNamePrefix="select"
        />
        <input
          type="number"
          value={this.state.param1}
          onChange={e => {
            this.setState({ param1: parseInt(e.target.value, 10) })
            onChange && onChange(variable, distname, param1, param2, param3)
          }
          }
        />
        <input
          type="number"
          step="0.1"
          value={this.state.param2}
          onChange={e => {
            this.setState({ param2: parseFloat(e.target.value, 10) })
            onChange && onChange(variable, distname, param1, param2, param3)
          }
          }
        />
        <input
          type="number"
          step="0.1"
          value={this.state.param3}
          onChange={e => {
            this.setState({ param3: parseFloat(e.target.value, 10) })
            onChange && onChange(variable, distname, param1, param2, param3)
          }
          }
        />

        <Plot
          data={this.plotData(data)}
        />
      </div>
    )
  }
}

export default compose(
)(Distribution)
