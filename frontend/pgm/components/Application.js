import * as SRD from "storm-react-diagrams";
import { AdvancedLinkModel, AdvancedPortModel, AdvancedLinkSegment, AdvancedLinkFactory } from '.'
/**
 * @author Dylan Vorster
 * @source https://github.com/projectstorm/react-diagrams
 */
export default class Application {

	constructor() {
		this.diagramEngine = new SRD.DiagramEngine();
		this.diagramEngine.installDefaultFactories();
		this.diagramEngine.registerLinkFactory(new AdvancedLinkFactory());

		this.newModel();
	}

	newModel() {
		this.activeModel = new SRD.DiagramModel();
		this.diagramEngine.setDiagramModel(this.activeModel);

		var node1 = new SRD.DefaultNodeModel("Action", "rgb(192,64,32)");
		node1.addInPort("X");
		node1.setPosition(100, 100);

		this.activeModel.addAll(node1);
	}

	getActiveDiagram() {
		return this.activeModel;
	}

	getDiagramEngine() {
		return this.diagramEngine;
	}
}