import React from 'react'

import classnames from 'classnames'
import { NavLink, DateTime } from 'components'
import { ROUTES } from 'routes'

export default class PGMLink extends React.Component {
  constructor(props) {
    super(props);
    this.state = { active: true };
  }

  click() {
    this.setState({ active: !this.state.active });
  }

  render() {
    const { active, pgm } = this.props
    let classes = classnames('pgm-link', { active: this.state.active });
    return (
      active
        ? <strong>{pgm}</strong>
        :
        <strong><NavLink className={classes} to={ROUTES.Modeler} params={{ id: pgm }}>
          {pgm}
        </NavLink></strong>
    )
  }
}