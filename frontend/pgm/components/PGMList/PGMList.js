import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { NavLink } from 'components'
import { ROUTES } from 'routes'
import PGMLink from 'comps/pgm/components/PGMLink'
import { listPGMs, newPGM } from 'comps/pgm/actions'
import { selectPGMsList } from 'comps/pgm/reducers/pgms'
import { ALL } from 'comps/pgm/constants'


import './pgm-list.scss'

class PGMList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentWillMount() {
    this.props.listPGMs.maybeTrigger()
  }

  createButton = () => {
    const { error, pristine, submitting } = this.props
    return (<div className="row">
      <button type="submit"
        className="button-primary"
        onClick={(e) => {
          this.props.newPGM.trigger({
          })
          this.props.listPGMs.trigger()
        }}
        disabled={pristine || submitting}
      >
        {submitting ? 'Creating...' : 'new Model'}
      </button>
    </div>)
  }

  render() {
    const { pgms } = this.props
    if (pgms.length === 0) {
      return (<div><p>No pgms yet.</p>{this.createButton()}</div>)
    }
    return (
      <div>
        <ul className="pgms" >
          <div>
            {pgms.map((pgm, i) => {
              return (
                <li key={i}>
                  <PGMLink name={pgm} pgm={pgm} key={i} active={false} />
                </li>
              )
            }
            )}
          </div>
        </ul>
        {this.createButton()}
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/pgm/reducers/pgms'))
const withSaga = injectSagas(require('comps/pgm/sagas/pgms'))
const withSaga2 = injectSagas(require('comps/pgm/sagas/newPGM'))

const withConnect = connect(
  (state) => {
    const pgms = selectPGMsList(state)
    return {
      pgms
    }
  },
  (dispatch) => bindRoutineCreators({ newPGM, listPGMs }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withSaga2,
  withConnect,
)(PGMList)
