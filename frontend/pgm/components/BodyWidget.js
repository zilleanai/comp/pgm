import * as _ from "lodash";
import * as React from "react";
import { DefaultNodeModel, DiagramWidget } from "storm-react-diagrams";
import { TrayItemWidget, TrayWidget } from ".";

/**
 * @author Dylan Vorster
 * @source https://github.com/projectstorm/react-diagrams
 */
export default class BodyWidget extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		const { items } = this.props
		const engine = this.props.app.getDiagramEngine()
		return (
			<div className="body">
				<div className="content">

					<TrayWidget>
						{/*<TrayItemWidget model={{ type: "in" }} name="Action" color="rgb(192,64,32)" />*/}

						{items.map((item, i) => {
							return (
								<TrayItemWidget key={i} model={{ type: "in_out" }} name={item} color="rgb(0,192,255)" />
							)
						}
						)}
					</TrayWidget>
					<div
						className="diagram-layer"
						onDrop={event => {
							var data = JSON.parse(event.dataTransfer.getData("storm-diagram-node"));
							var nodesCount = _.keys(
								this.props.app
									.getDiagramEngine()
									.getDiagramModel()
									.getNodes()
							).length;

							var node = null;
							if (data.type === "in") {
								node = new DefaultNodeModel(data.name + " " + (nodesCount + 1), "rgb(192,64,32)");
								node.addInPort("X");
							} else if (data.type ==="out") {
								node = new DefaultNodeModel(data.name + " " + (nodesCount + 1), "rgb(0,192,255)");
								node.addOutPort("O");
							}else {
								node = new DefaultNodeModel(data.name + " " + (nodesCount + 1), "rgb(0,192,255)");
								node.addInPort("X");
								node.addOutPort("O");
							}
							var points = engine.getRelativeMousePoint(event);
							node.x = points.x;
							node.y = points.y;
							this.props.app
								.getDiagramEngine()
								.getDiagramModel()
								.addNode(node);
							this.forceUpdate();
						}}
						onDragOver={event => {
							event.preventDefault();
						}}
					>
						<DiagramWidget className="srd-demo-canvas" diagramEngine={engine} />
					</div>
				</div>
			</div>
		);
	}
}
