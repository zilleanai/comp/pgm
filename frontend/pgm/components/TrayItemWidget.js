import * as React from "react";

/**
 * @author Dylan Vorster
 * @source https://github.com/projectstorm/react-diagrams
 */
export default class TrayItemWidget extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div
                style={{ borderColor: this.props.color }}
                draggable={true}
                onDragStart={event => {
                    var data = this.props.model
                    data.name = this.props.name
                    event.dataTransfer.setData("storm-diagram-node", JSON.stringify(data));
                }}
                className="tray-item"
            >
                {this.props.name}
            </div>
        );
    }
}
